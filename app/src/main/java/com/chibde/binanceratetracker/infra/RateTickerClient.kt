package com.chibde.binanceratetracker.infra

import com.chibde.binanceratetracker.model.TickerData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.*

/**
 * Created by gautam on 14/4/18.
 */
class RateTickerClient(private val client: OkHttpClient = OkHttpClient().newBuilder().build(),
                       private val rateRateTrackerCallback: RateTrackerCallback,
                       private val symbol: String,
                       private val purchase: Double) : WebSocketListener() {

    private val gson = Gson()
    private val type = object : TypeToken<TickerData>() {}.type
    private var ticker: Int = 0
    fun setUpSocket() {
        client.newWebSocket(Request.Builder()
                .url(BASE_SOCKET_URL + symbol + ATTR)
                .build(), this)
    }

    fun stop() {
        client.dispatcher().executorService().shutdown()
    }

    override fun onMessage(webSocket: WebSocket, text: String) {
        val tickerData: TickerData = gson.fromJson(text, type)
        val percentage = (tickerData.rate.toDouble().minus(purchase))
                .div(tickerData.rate.toDouble())
                .times(100)
        percentage.toInt().let {
            if (ticker != it) {
                ticker = it
            } else {
                rateRateTrackerCallback.sendNotification(tickerData.symbol, percentage)
            }
        }
    }

    override fun onClosing(webSocket: WebSocket, code: Int, reason: String) {
        rateRateTrackerCallback.onFailure("Closing :  $code  //  $reason")
    }

    override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response) {
        rateRateTrackerCallback.onFailure(t.message)
    }

    companion object {
        const val BASE_SOCKET_URL = "wss://stream.binance.com:9443/ws/"
        const val ATTR = "@aggTrade"
    }
}