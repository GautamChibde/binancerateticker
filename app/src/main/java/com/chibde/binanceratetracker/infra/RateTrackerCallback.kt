package com.chibde.binanceratetracker.infra

/**
 * Created by gautam on 14/4/18.
 */
interface RateTrackerCallback {
    fun sendNotification(symbol: String, percentage: Double)
    fun onFailure(message: String?)
}