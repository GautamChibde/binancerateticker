package com.chibde.binanceratetracker.main

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.support.v4.app.NotificationCompat
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.chibde.binanceratetracker.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainActivityView {
    private val presenter: MainActivityPresenter by lazy { MainActivityPresenter(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        activity_main_btn_track.setOnClickListener {
            presenter.startTracking(
                    activity_main_symbol.text.toString(),
                    activity_main_tracking_rate.text.toString()
            )
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDetach()
    }

    override fun sendNotification(symbol: String, percentage: Double) {
        val mBuilder = NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_bitcoin_24px)
                .setContentTitle("rate alert")
                .setChannelId(CHANNEL_ID)
                .setContentText("$symbol changed $percentage%")
        val notificationManager = (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager.createNotificationChannel(NotificationChannel(
                    CHANNEL_ID,
                    CHANNEL,
                    NotificationManager.IMPORTANCE_HIGH))
            notificationManager.notify(0, mBuilder.build())
        } else {
            notificationManager.notify(0, mBuilder.build())
        }
    }

    override fun onFailure(message: String?) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun showErrorSymbolEmpty() {
        activity_main_symbol.error = getString(R.string.activity_main_symbol_empty)
        activity_main_symbol.requestFocus()
    }

    override fun showErrorRateEmpty() {
        activity_main_tracking_rate.error = getString(R.string.activity_main_rate_empty)
        activity_main_tracking_rate.requestFocus()
    }

    companion object {
        const val CHANNEL = "binance"
        const val CHANNEL_ID = "binance_id"
    }
}

