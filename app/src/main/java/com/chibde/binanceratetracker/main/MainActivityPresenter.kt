package com.chibde.binanceratetracker.main

import com.chibde.binanceratetracker.infra.RateTickerClient
import com.chibde.binanceratetracker.infra.RateTrackerCallback

/**
 * Created by gautam on 16/4/18.
 */
class MainActivityPresenter(private val view: MainActivityView) : RateTrackerCallback {
    override fun sendNotification(symbol: String, percentage: Double) {
        view.sendNotification(symbol, percentage)
    }

    private lateinit var socket: RateTickerClient

    fun startTracking(symbol: String, rate: String) {
        when {
            symbol.isBlank() -> view.showErrorSymbolEmpty()
            rate.isBlank() -> view.showErrorRateEmpty()
            else -> setupSocket(symbol, rate.toDouble())
        }
    }

    private fun setupSocket(symbol: String, rate: Double) {
        socket = RateTickerClient(rateRateTrackerCallback = this,
                symbol = symbol,
                purchase = rate)
        socket.setUpSocket()
    }

    override fun onFailure(message: String?) {
        view.onFailure(message)
    }

    fun onDetach() {
        socket.stop()
    }
}