package com.chibde.binanceratetracker.main

/**
 * Created by gautam on 16/4/18.
 */
interface MainActivityView {
    fun showErrorSymbolEmpty()
    fun showErrorRateEmpty()
    fun sendNotification(symbol: String, percentage: Double)
    fun onFailure(message: String?)
}