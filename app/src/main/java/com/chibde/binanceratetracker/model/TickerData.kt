package com.chibde.binanceratetracker.model

import com.google.gson.annotations.SerializedName

/**
 * Created by gautam on 14/4/18.
 */
data class TickerData(@SerializedName("s") var symbol:String,
                      @SerializedName("p") var rate : String)